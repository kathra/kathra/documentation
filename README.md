# User documentation

## Core concepts

[Explain how Kathra are designed](./coreConcepts/coreConcepts.md)

## How to install an instance

To install Kathra, we can run it on a Kubernetes Cluster or a local instance using Minikube.

For more information, see https://gitlab.com/kathra/deployment

## Generate a new component

[How to create a new component from Swagger File](./createComponent/createComponent.md)

[Java guidelines](./development/javaGuide.md)

[Python guidelines](./development/pythonGuide.md)

