## Python development: getting started

### Prerequisites

You need to install the following components to build and run Python projects generated by KATHRA.

#### Python 3.6

To install Python 3.6, please follow the reference manual : https://wiki.python.org/moin/BeginnersGuide/Download


#### pip

To install pip, please follow the reference manual : https://pip.pypa.io/en/stable/installing/

Edit the pip.conf configuration file to add the KATHRA repository (to find pip.conf on your system, see : https://pip.pypa.io/en/stable/user_guide/#config-file):


	[global]
	...
	extra-index-url = [nexus_http_url]/repository/pip-snapshots/simple


[nexus_http_url] refers to the Nexus instance deployed for KATHRA.

### Build a Python project

To build a Python project, please type this command at the root source folder of the project :

`pip install -r [projectName]/requirements.txt --no-cache-dir` 

### Run a Python project
To run a project, please make sure that the 8080 port is avalaible, since it's used to expose the services.

Run the following command :

`python launcher.py`

You can access to the running services by this URL : http://localhost:8080/ui


