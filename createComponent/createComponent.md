## Tutorial : create an application with KATHRA

### Prerequisites

* Please read the [following document](../coreConcepts/coreConcepts.md) to know the core concepts of KATHRA Applications. 
* Make sure you are a member of, at least, one team in KATHRA. If you have no team, please contact your KATHRA administrators.
* You must have logged in at least once on the Gitlab instance used by KATHRA. 

### Procedure

Connect to the dashboard :
![](1-main.png)

Click on "Applications" :
![](2-application.png)

Click on "Create a new Component" :
![](3-createComponent.png)

Write a name, description and select a team : this team will be the component's owner. Click on "Validate" :
![](4-createComponent.png)

The component creation my take few second. When its status is "READY", click on "Add API" :
![](5-newApi.png)

To add an API to your component, you need a OpenApi 2.0 file, in YAML format. To describe an API, see the [documentation here](https://swagger.io/docs/specification/2-0/basic-structure).

Important : in your YAML file, you will need to declare an additional "x-groupId" parameter for your API. This parameter will be used to generate the folder arborenscence of your implementation.

The x-groupId arguments follows the [Java package nomenclature](https://docs.oracle.com/javase/tutorial/java/package/namingpkgs.html) (ex : org.mycompany.myteam) and must be declared like this :

	swagger: '2.0'
		info:
	  		[..]
			x-groupId: <value>

You can also start from the [KATHRA "quickstart.yaml" file](quickstart.yaml) as a basic example. 

If the uploaded file is OK, you can create your API by clicking on "Validate" :
![](6-newApi.png)

The API creation takes several minutes, since the core libraries are generated. Please note that you can navigate in menus during the creation process : you get a notification when the API is ready.
![](7-newApi.png)

When an API is ready, it appears as a version in the component list :
![](8-components.png)

By clicking on the component, you can see the details, including the OpenAPI 2.0 file that have been used :
![](9-componentDetail.png)

Please note that, at this point, you only decribed the endpoints of your component, not the way it will be implemented.

You can generate an implementation in a specific langage. Click on "New API Implementation" :
![](10-newImplementation.png)

Type an name, a decription and select a team and a desired language for your implementation, and click on "Validate" :
![](11-newImplementation.png)

Like the API creation, you can navigate in menus during the implementation creation, and get notified when it's ready.
![](12-newImplementation.png)

After the implementation is created, it appears in the implementations list of the "Applications" menu :
![](13-implementations.png)

You can access to the details of an implementation by clicking on it :
![](14-implementations.png)

You can begin to develop your implementation : get its source code by a "git clone [..]" command using the provided git URL , and refer to the developer's guide of your implementation langage.

[Java development : getting started](../development/javaGuide.md) 

[Python development : getting started](../development/pythonGuide.md)
