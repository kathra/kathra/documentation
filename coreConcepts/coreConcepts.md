# Kathra core concepts

### Applications

Kathra allows developers to generate micro-services designed applications. There are several concepts to understand before creating your first application in Kathra.

#### Components
The component is the core entity. A component has a name, a description and an API describing the services provided by the component.

As an example, we can define a very simple "Calculator" component, with the following API :
```
Name : Calculator 
Description : Basic operations

API V1.0.0 :
	/addition : 
		description : adds two numbers
		parameters :
        	- name : firstNumber
        	  type : int
        	- name : secondNumber
			  type : int
		response :
			type : int

	/substraction :
		description : adds two numbers
		parameters :
	    	- name : firstNumber
	    	  type : int
	    	- name : secondNumber
			  type : int
		response :
			type : int

```

#### Implementations

An implementation is a micro-service application. It can be deployed as an application on the Kathra environment.

It is always related to a component's API : when deployed, any service described in the component's API is accessible in HTTP (or HTTPS). 

We can have severals implementations for the same API, with different names. Every implementations have its own behaviour and programmatic language, but must respect the API definitions.

The following schema summarizes the relationships between components and implementations :

![](applicationRelashionships.png)



#### General creation process

In the creation process, the mandatory steps to create an application are :
1. create a component, with an API
2. create an implementation from this component, by selecting a programmatic language
3. gets the implementation generated source code, and develop every service specified in the API
4. push the implementation source code, and launch a build to deploy the implementation on the Kathra environment

To know how to do this with Kathra, please refer to [this document](../createComponent/createComponent.md).

### Environnements

Kathra allows users to deploy applications on execution environments. Once deployed, applications are generally accessibles via HTTP (or HTTPS) endpoints.

#### General creation process

In the creation process, the mandatory steps to create an environment are :
1. Choose a name for the target environement
2. Select one or more applications to deploy from the catalog
3. Set the applications parameters, if requested (ex : default admin password if the application is a database)
4. Launch the environment creation to deploy the applications.

### Catalog

The Kathra catalog allows users to consult the applications that can be deployed on environments.
